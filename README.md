# Design Challenge 

#### Create Customer list & customer detail page/pages as per your creativity & design knowledge,

  * fields to be shown in customer lists page is - image, name, email, location
  * Functionality
    - customers can be searched via name, email, location
    - user can restrict customers per page (ex: 5, 10 ,15)
  * when user clicks customer summary it's show its details

  * navigation contains this menu's `dashboard`, `contacts`, `customers`, `settings` 

Here is the json file which contain customers data `customers.json`